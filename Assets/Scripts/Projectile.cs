﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] float damage = 50;
    [SerializeField] float speed = 10;

    Vector2 startScale = new Vector2(0.5f, 0.5f);
    Vector2 normalScale = new Vector2(1, 1);
    Vector2 rotatePan = new Vector2(0, -6);
    Vector3 rotateAxe = new Vector3(0, 0, -4);
    Rigidbody2D moveProjectile;
    float projectileTimer;

    private void Start()
    {
        transform.localScale = startScale;
        moveProjectile = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        PanAnimation();
        AxeAnimation();
    }

    private void PanAnimation()
    {
        if (tag == "Pan")
        {
            projectileTimer += Time.deltaTime;
            // transform.Translate(Vector2.right * speed * Time.deltaTime);
            moveProjectile.velocity = new Vector2(speed, 0);
            if (projectileTimer > 0.1f)
            {
                transform.localScale = normalScale;
                transform.Rotate(rotatePan);
            }
        }     
    }

    private void AxeAnimation()
    {
        if (tag == "Axe")
        {
            projectileTimer += Time.deltaTime;          
            moveProjectile.velocity = new Vector2(speed, 0);
            if (projectileTimer > 0.1f)
            {
                transform.localScale = normalScale;
                transform.Rotate(rotateAxe);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D otherCollider)
    {
        Health health = otherCollider.GetComponent<Health>();
        Attacker attacker = otherCollider.GetComponent<Attacker>();

        if (attacker && health) //если коллайдеры имеют эти компоненты
        {
            health.DealDamage(damage);
            Destroy(gameObject);
        }       
    }
}
