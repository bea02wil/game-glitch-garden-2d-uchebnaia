﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attacker : MonoBehaviour
{
    [Range(0, 5f)] float currentSpeed = 1f;
    GameObject currentTarget;
    Animator animator;

    private void Awake()
    {
        FindObjectOfType<LevelController>().AttackerSpawned();
        animator = GetComponent<Animator>();
    }

    private void OnDestroy()
    {
        LevelController levelController = FindObjectOfType<LevelController>();
        if (levelController != null)
        {
            levelController.AttackerKilled();
        }

    }

    void Update()
    {
        transform.Translate(Vector2.left * currentSpeed * Time.deltaTime); //чтобы фрейм независимо, одинаковая скорость
        UpdateAnimationState();
    }

    public void SetMovementSpeed(float speed)
    {
        currentSpeed = speed;
    }

    public void Attack(GameObject target)
    {
        animator.SetBool("isAttacking", true);
        currentTarget = target;
    }

    public void StrikeCurrentTarget(float damage)
    {
        if (!currentTarget) { return; } //если нет карренттаргета, то есть он не присвоен
        Health health = currentTarget.GetComponent<Health>();
        if (health) //если объект с которым сталкивается лизард имеет компонет health
        {
            health.DealDamage(damage);
        }
    }

    private void UpdateAnimationState()
    {
        if (!currentTarget) //тоже самое что currentTarget == null
        {
            animator.SetBool("isAttacking", false);
        }
    }
}
